import 'package:flutter/material.dart';

class Gift extends StatelessWidget {
  const Gift({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Icon(
          Icons.gif_box,
          size: 50,
        ),
      ),
    );
  }
}

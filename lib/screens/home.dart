import 'package:facebook_ui/screens/gift.dart';
import 'package:facebook_ui/screens/menu.dart';
import 'package:facebook_ui/screens/profile.dart';
import 'package:facebook_ui/screens/search.dart';
import 'package:facebook_ui/screens/video.dart';
import 'package:flutter/material.dart';

import 'notification.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 6,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "facebook",
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.bold,
            ),
          ),
          actions: [
            Container(
              height: 45,
              width: 45,
              decoration: BoxDecoration(
                color: Colors.black45,
                shape: BoxShape.circle,
              ),
              child: IconButton(
                onPressed: () {},
                icon: Icon(
                  Icons.search,
                  size: 28,
                ),
              ),
            ),
            SizedBox(width: 5),
            Container(
              height: 45,
              width: 45,
              decoration: BoxDecoration(
                color: Colors.black45,
                shape: BoxShape.circle,
              ),
              child: Center(
                child: IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.facebook,
                    size: 28,
                  ),
                ),
              ),
            ),
          ],
          bottom: TabBar(
            tabs: [
              Tab(
                icon: Icon(Icons.home, size: 30),
              ),
              Tab(
                icon: Icon(Icons.person_outline_outlined, size: 30),
              ),
              Tab(
                icon: Icon(Icons.video_settings_outlined, size: 30),
              ),
              Tab(
                icon: Icon(Icons.gif_box_outlined, size: 30),
              ),
              Tab(
                icon: Icon(Icons.notifications, size: 30),
              ),
              Tab(
                icon: Icon(Icons.menu, size: 30),
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            SearchBar(),
            Profile(),
            Videos(),
            Gift(),
            Notifications(),
            Menu(),
          ],
        ),
      ),
    );
  }
}
